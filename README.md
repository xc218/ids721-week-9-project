# Ids721 Week 9 Project

## Demo Link

https://ids-mp9-4tmylnczcp5s33zxdvy2n4.streamlit.app/

## Steps
### Setting Up Environment
* Install Streamlit
`pip install streamlit`
* Install Hugging Face Transformers
`pip install transformers`

### Create Streamlit Web App
* Create a new Python file (`app.py`)
* Import Libraries
```
import streamlit as st
from transformers import pipeline
```

* Setup UI
```
st.title('My AI Chatbot')
user_input = st.text_input("Talk to the chatbot:")
```

* Load Hugging Face Model
```
chat = pipeline('text-generation', model='microsoft/DialoGPT-medium')
```

* Process User Input
```
if user_input:
    response = chat(user_input)[0]['generated_text']
    st.text_area("Chatbot says:", value=response, height=100, max_chars=None, key=None)
```

* Run Your App
Navigate to the directory containing `app.py` and run `streamlit run app.py`

![1](1.png)

### Deploy
* Sign up on https://streamlit.io/

* Push `app.py` to `Github`

* Create a new app using `Github` repository

* Deploy

![deploy](deploy.png)
### Result
![result](result.png)